import { Module } from "@nestjs/common";
import { NestFactory } from "@nestjs/core";
import { GraphQLModule } from "@nestjs/graphql";
import { ApolloDriver, ApolloDriverConfig } from "@nestjs/apollo";

import { ConfigModule } from "@uiii-lib/nestjs-config";
import { SessionModule } from "@uiii-lib/nestjs-session";
import { PgStoreSessionOptions } from "@uiii-lib/nestjs-session/store/pg";
import { DatabaseModule } from "@uiii-lib/nestjs-database-postgres";

import { BaseAuthModule } from "../../../src/base-auth.module.js";

import { AuthContext } from "./auth-context.js";
import { CognitoAuthenticator } from "./cognito.authenticator.js";
import { LocalAuthenticator } from "./local.authenticator.js";
import { TestConfig } from "./test.config.js";
import { TestController } from "./test.controller.js";
import { TestResolver } from "./test.resolver.js";

@Module({
	imports: [
		ConfigModule,
		SessionModule.forRootAsync<PgStoreSessionOptions>({
			storeType: "pg",
			useFactory: (config: TestConfig) => ({
				secret: config.sessionSecret,
				secureCookie: config.sessionSecureCookie,
				storeOptions: {
					initDb: config.sessionStoreInitDb,
					schemaName: config.sessionStoreSchemaName,
					tableName: config.sessionStoreTableName,
				},
			}),
			inject: [TestConfig],
			provideInjectionTokensFrom: [TestConfig],
		}),
		DatabaseModule,
		GraphQLModule.forRoot<ApolloDriverConfig>({
			driver: ApolloDriver,
			autoSchemaFile: true,
			context: ({ req }: any) => ({ req }),
		}),
		BaseAuthModule.forRoot({
			context: AuthContext,
		}),
	],
	controllers: [TestController],
	providers: [
		TestConfig,
		LocalAuthenticator,
		CognitoAuthenticator,
		TestResolver,
	],
})
export class MainModule {}

async function bootstrap() {
	const app = await NestFactory.create(MainModule);
	await app.listen(3000);
}

bootstrap();
