import { Req, UseGuards } from "@nestjs/common";
import {
	Args,
	Context,
	Mutation,
	ObjectType,
	Query,
	Resolver,
} from "@nestjs/graphql";

import { AuthGuard } from "../../../src/auth.guard.js";
import { CurrentAuth } from "../../../src/current-auth.decorator.js";
import { AuthContextType } from "../../../src/auth.graphql.js";
import { SessionAuthContext } from "../../../src/session-auth.context.js";

import { AuthContext } from "./auth-context.js";
import { AuthData } from "./auth-data.js";
import { CognitoAuthenticator } from "./cognito.authenticator.js";
import { LocalAuthenticator } from "./local.authenticator.js";

@ObjectType()
class AuthContextWithData extends AuthContextType(AuthData) {}

@Resolver()
export class TestResolver {
	constructor(
		protected localAuthenticator: LocalAuthenticator,
		protected cognitoAuthenticator: CognitoAuthenticator,
	) {}

	@Mutation((returns) => String)
	async localLogIn(
		@Args("email") email: string,
		@Args("password") password: string,
		@CurrentAuth() auth: AuthContext,
	) {
		await this.localAuthenticator.authenticate(auth, email, password);
		return "ok";
	}

	@Mutation((returns) => String)
	async cognitoLogIn(
		@Args("email") email: string,
		@Args("password") password: string,
		@CurrentAuth() auth: AuthContext,
		@Context() ctx: any,
	) {
		this.cognitoAuthenticator.updateConfig(ctx.req);
		await this.cognitoAuthenticator.authenticate(auth, email, password);
		return "ok";
	}

	@Mutation((returns) => String)
	@UseGuards(AuthGuard)
	async logOut(@CurrentAuth() auth: SessionAuthContext) {
		await auth.logOut();
		return "ok";
	}

	@Query((returns) => AuthContextWithData)
	currentAuth(@CurrentAuth() auth: SessionAuthContext) {
		return auth;
	}
}
