import fs from "fs";
import path from "path";
import { Request } from "express";

import { Controller, Get, Query, Req, UseGuards } from "@nestjs/common";

import { SessionAuthContext } from "../../../src/session-auth.context.js";
import { CurrentAuth } from "../../../src/current-auth.decorator.js";
import { AuthGuard } from "../../../src/auth.guard.js";

import { AuthContext } from "./auth-context.js";
import { CognitoAuthenticator } from "./cognito.authenticator.js";
import { LocalAuthenticator } from "./local.authenticator.js";

@Controller()
export class TestController {
	constructor(
		protected localAuthenticator: LocalAuthenticator,
		protected cognitoAuthenticator: CognitoAuthenticator,
	) {}

	@Get()
	getIndex() {
		return fs.readFileSync(
			path.join(
				(process.env as any).APP_DIR,
				"test",
				"app",
				"src",
				"index.html",
			),
			"utf-8",
		);
	}

	@Get("/current-auth")
	getCurrentAuth(@CurrentAuth() auth: AuthContext) {
		return auth;
	}

	@Get("/login/local")
	async logInLocal(
		@Query("email") email: string,
		@Query("password") password: string,
		@CurrentAuth() auth: AuthContext,
	) {
		await this.localAuthenticator.authenticate(auth, email, password);
		return "ok";
	}

	@Get("/login/cognito")
	async logInCognito(
		@Query("email") email: string,
		@Query("password") password: string,
		@CurrentAuth() auth: AuthContext,
		@Req() req: Request,
	) {
		this.cognitoAuthenticator.updateConfig(req);
		await this.cognitoAuthenticator.authenticate(auth, email, password);
		return "ok";
	}

	@Get("/logout")
	@UseGuards(AuthGuard)
	async logOut(@CurrentAuth() auth: SessionAuthContext) {
		await auth.logOut();
		return "ok";
	}
}
