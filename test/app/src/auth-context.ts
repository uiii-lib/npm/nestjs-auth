import { DeepSelector } from "@uiii-lib/utils";

import { SessionAuthContext } from "../../../src/session-auth.context.js";

import { AuthData } from "./auth-data.js";

export class AuthContext extends SessionAuthContext<AuthData> {
	override publicDataSelector: DeepSelector<AuthData> = {
		email: true,
	};
}
