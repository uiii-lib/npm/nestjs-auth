import { Injectable } from "@nestjs/common";
import { ConfigService } from "@uiii-lib/nestjs-config";

@Injectable()
export class TestConfig {
	constructor(private config: ConfigService) {}

	get sessionSecret(): string {
		return this.config.get("SESSION_SECRET", true);
	}

	get sessionSecureCookie(): boolean {
		return this.config.get("SESSION_SECURE_COOKIE") === "true";
	}

	get sessionStoreInitDb(): boolean {
		return this.config.get("SESSION_STORE_INIT_DB") === "true";
	}

	get sessionStoreSchemaName(): string {
		return this.config.get("SESSION_STORE_SCHEMA", true);
	}

	get sessionStoreTableName(): string {
		return this.config.get("SESSION_STORE_TABLE", true);
	}
}
