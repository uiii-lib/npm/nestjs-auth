import { Injectable } from "@nestjs/common";
import { Request } from "express";

import { CognitoAuthenticator as BaseCognitoAuthenticator } from "../../../src/authenticator/cognito.authenticator.js";

import { AuthContext } from "./auth-context.js";
import { AuthData } from "./auth-data.js";

@Injectable()
export class CognitoAuthenticator extends BaseCognitoAuthenticator<AuthContext> {
	constructor() {
		super({
			userPoolId: "", // will be set by cookie
			clientId: "", // will be set by cookie
		});
	}

	updateConfig(req: Request) {
		this.userPoolId = this.getCookieValue("cognito_pool", req.headers.cookie);
		this.clientId = this.getCookieValue("cognito_client", req.headers.cookie);
	}

	protected override createAuthData(email: string): AuthData {
		return { email };
	}

	protected getCookieValue(name: string, cookieString?: string) {
		return (
			cookieString?.match("(^|;)\\s*" + name + "\\s*=\\s*([^;]+)")?.pop() || ""
		);
	}
}
