import { Field, ObjectType } from "@nestjs/graphql";

@ObjectType()
export class AuthData {
	@Field()
	email: string;
}
