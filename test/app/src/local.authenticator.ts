import { Injectable, UnauthorizedException } from "@nestjs/common";

import { Authenticator } from "../../../src/authenticator/authenticator.js";

import { AuthContext } from "./auth-context.js";

@Injectable()
export class LocalAuthenticator extends Authenticator<AuthContext> {
	override async authenticate(
		authContext: AuthContext,
		email: string,
		password: string,
	) {
		if (email !== "test@example.com" || password !== "pa$$W0rd") {
			throw new UnauthorizedException("Invalid credentials");
		}

		console.log("local auth", { email });
		await authContext.logIn({ email });
	}
}
