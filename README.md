# Auth module for NestJS

Authentication module for NestJS

## Requirements

This module requires a **session**. Recommended way is to use [`@uiii-lib/nestjs-session`](https://gitlab.com/uiii-lib/npm/nestjs-session) package.

## Usage (server)

Register `@uiii-lib` package registry to npm

```
npm config set @uiii-lib:registry https://gitlab.com/api/v4/packages/npm/
```

Install the package

> Requires [`@nestjs/graphql`](https://github.com/nestjs/graphql) as peer dependency, so install too

```
npm install --save @uiii-lib/nestjs-auth
```

### Auth Context

First define your auth context. Auth context is an instance of a class which manage data indentifying the authenticated user.

It has these methods and properties:
- `async logIn(data)` - Method to log in the user and set the data identifying it
- `async logOut()` - Method to log out the user, clearing the auth context
- `isLoggedIn()` - Method returning a boolean indicating if the user is logged-in
- `data` - Identifying data of the logged-in user

Each app must have own custom auth context defined. It must derive `BaseAuthContext` class, there is also avaiable `SessionAuthContext` for session based authentication.

`BaseAuthContext` requires one generic argument which describes the shape of auth data. If you wan't to expose some data to the client side, override the `publicDataSelector` property in the derived auth context class. Data will be filtered automatically on auth context converting to JSON (see `toJSON` method).

In controllers and resolved the auth context is retrived by `CurrentAuth` decorator.

```ts
// auth-context.ts

import { SessionAuthContext } from "@uiii-lib/nestjs-auth";
import { DeepSelector } from "@uiii-lib/utils";

export interface AuthData {
	accessKey: string;
	email: string;
}

export class AuthContext extends SessionAuthContext<AuthData> {
	override publicDataSelector: DeepSelector<AuthData> = {
		email: true, // expose only email field
	};
}
```

### Authenticator

The next thing you need to define is an authenticator. It should extends abstract `Authenticator` class and implement `authenticate` method which verifies the validity of the user's credentials and call `AuthContext.logIn(data)` on success. Otherwise throw an `UnauthorizedException`. `Authenticator` base class requires auth context type as a generic argument.

```ts
// local.authenticator.ts

import { Injectable, UnauthorizedException } from "@nestjs/common";

import { Authenticator } from "@uiii-lib/nestjs-auth";

import { AuthContext } from "./auth-context.js";

@Injectable()
export class LocalAuthenticator extends Authenticator<AuthContext> {
	override authenticate(authContext: AuthContext, email: string, password: string): string {
		if (email !== 'test@example.com' || password !== 'pa$$W0rd') {
			throw new UnauthorizedException("Invalid credentials");
		}

		await authContext.logIn(email);
	}
}
```

You can also extends one of pre-implemented authenticators:

- `CognitoAuthenticator` - verifies the login credentials with AWS Cognito user pool

    > Options:
    >   - `userPoolId` - ID of AWS Cognito user pool
    >   - `clientId` - ID of AWS Cognito client

### Auth Module

Next create the main auth module. Import the `SessionAuthModule` from this package. And add your authenticator into providers.

```ts
import { Module } from '@nestjs/common';

import { BaseAuthModule } from '@uiii-lib/nestjs-auth';

import { AuthContext } from './auth-context.js';
import { LocalAuthenticator } from './local.authenticator.js';

@Module({
	imports: [
		BaseAuthModule.forRoot({
			context: AuthContext,
		}),
	]
	providers: [LocalAuthenticator],
	exports: [LocalAuthenticator]
})
export class AuthModule {}
```

### Controllers & Resolvers

Then you can create a log in and log out endpoints in your controllers or resolvers.


```ts
import { Resolver, Query, Args, Mutation } from '@nestjs/graphql';
import { UseGuards } from '@nestjs/common';
import { AuthGuard, CurrentAuth } from '@uiii-lib/nestjs-auth';

import { AuthContext } from './auth-context.js';
import { LocalAuthenticator } from './local.authenticator.js';

@Resolver()
export class AuthResolver {
	constructor(protected localAuthenticator: LocalAuthenticator) {}

	@Mutation(returns => String)
	async logIn(@Args('email') email: string, @Args('password') password: string, @CurrentAuth() auth: AuthContext) {
		await this.localAuthenticator.authenticate(auth, email, password);
		return "ok";
	}

    @Mutation(returns => String)
	@UseGuards(AuthGuard)
	async logIn(@CurrentAuth() auth: AuthContext) {
		await auth.logOut();
		return "ok";
	}

	@Query(returns => AuthContext)
	currentAuth(@CurrentAuth() auth: AuthContext) {
		return auth;
	}
}
```

### Guards

Use `AuthGuard` to protect endpoint from unauthenticated users.

```ts
import { Resolver, Mutation } from '@nestjs/graphql';
import { UseGuards } from '@nestjs/common';
import { AuthGuard, CurrentAuth } from '@uiii-lib/nestjs-auth';

import { AuthContext } from './auth-context.js';
import { DataService } from './data.service.js';

@Resolver()
export class DataResolver {
	constructor(protected dataService: DataService) {}

	@Mutation(returns => String)
	@UseGuards(AuthGuard)
	async protectedData(@CurrentAuth() auth: AuthContext) {
		return this.dataService.getDataForUser(auth.id);
	}
}
```

### GraphQL

If you use GraphQL do not forget to include request into the GraphQL context

```ts
@Module({
	imports: [
		...
		GraphQLModule.forRoot({
			autoSchemaFile: true,
			context: ({ req }) => ({ req }), // <--- THIS
			installSubscriptionHandlers: true
		}),
		...
	]
})
export class MainModule {}
```
