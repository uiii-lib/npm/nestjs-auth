FROM node:20-slim

WORKDIR /usr/src/app

RUN apt-get update && apt-get -y install procps vim

ENV DEBUG_OPTIONS 0.0.0.0:9229

CMD npm install && \
	cd test/app && \
	npm install && \
	npm run start:debug
