import { BaseAuthContext } from "../base-auth.context.js";

export abstract class Authenticator<
	C extends BaseAuthContext<NonNullable<any>>,
> {
	abstract authenticate(authContext: C, ...args: any[]): Promise<void>;
}
