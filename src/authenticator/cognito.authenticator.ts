import {
	CognitoUserPool,
	CognitoUser,
	AuthenticationDetails,
} from "amazon-cognito-identity-js";

import { Injectable, UnauthorizedException } from "@nestjs/common";

import { BaseAuthContext } from "../base-auth.context.js";

import { Authenticator } from "./authenticator.js";

export interface CognitoAuthenticatorOptions {
	userPoolId: string;
	clientId: string;
}

@Injectable()
export abstract class CognitoAuthenticator<
	C extends BaseAuthContext<NonNullable<any>>,
> extends Authenticator<C> {
	protected userPoolId: string;
	protected clientId: string;

	protected pool: CognitoUserPool | undefined;

	constructor(options: CognitoAuthenticatorOptions) {
		super();

		this.userPoolId = options.userPoolId;
		this.clientId = options.clientId;
	}

	override async authenticate(
		authContext: C,
		email: string,
		password: string,
	): Promise<void> {
		const cognitoUser = new CognitoUser({
			Pool: this.getCognitoUserPool(),
			Username: email,
		});

		const authDetails = new AuthenticationDetails({
			Username: email,
			Password: password,
		});

		try {
			await new Promise((resolve, reject) => {
				cognitoUser.authenticateUser(authDetails, {
					onSuccess: resolve,
					onFailure: reject,
				});
			});
		} catch (e: any) {
			throw new UnauthorizedException(e.message);
		}

		await authContext.logIn(await this.createAuthData(email));
	}

	protected abstract createAuthData(
		email: string,
	): C extends BaseAuthContext<infer D> ? D : never;

	protected getCognitoUserPool() {
		if (!this.pool) {
			this.pool = new CognitoUserPool({
				UserPoolId: this.userPoolId,
				ClientId: this.clientId,
			});
		}

		return this.pool;
	}
}
