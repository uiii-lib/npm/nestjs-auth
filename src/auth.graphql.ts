import { Type } from "@nestjs/common";
import { Field, ObjectType } from "@nestjs/graphql";

export function AuthContextType<T>(AuthDataType: Type<T>) {
	@ObjectType()
	class AuthContext {
		@Field()
		isLoggedIn: boolean;

		@Field(() => AuthDataType)
		data: T;
	}

	return AuthContext;
}
