import "express-session";

import { BaseAuthContext } from "./base-auth.context.ts";

declare global {
	namespace Express {
		interface Request {
			auth: BaseAuthContext;
		}
	}
}

declare module "express-session" {
	interface Session {
		auth: unknown | undefined;
	}
}
