import {
	DynamicModule,
	MiddlewareConsumer,
	Module,
	Type,
} from "@nestjs/common";

import { AUTH_CONTEXT_CLASS, BaseAuthContext } from "./base-auth.context.js";

import { AuthMiddleware } from "./auth.middleware.js";

export interface BaseAuthModuleOptions {
	context: Type<BaseAuthContext<any>>;
}

@Module({})
export class BaseAuthModule {
	static forRoot(options: BaseAuthModuleOptions): DynamicModule {
		return {
			module: BaseAuthModule,

			providers: [
				AuthMiddleware,
				{
					provide: AUTH_CONTEXT_CLASS,
					useValue: options.context,
				},
			],
		};
	}

	async configure(consumer: MiddlewareConsumer) {
		consumer.apply(AuthMiddleware).forRoutes("*");
	}
}
