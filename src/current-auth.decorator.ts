import { createParamDecorator, ExecutionContext } from "@nestjs/common";

import { BaseAuthContext } from "./base-auth.context.js";
import { getRequest } from "./helpers.js";

export const CurrentAuth = createParamDecorator<
	never,
	ExecutionContext,
	BaseAuthContext<any>
>((data, ctx) => {
	const req = getRequest(ctx);
	return req.auth;
});
