import { Request } from "express";

import { Inject, Injectable, NestMiddleware, Type } from "@nestjs/common";

import { AUTH_CONTEXT_CLASS, BaseAuthContext } from "./base-auth.context.js";

@Injectable()
export class AuthMiddleware implements NestMiddleware {
	constructor(
		@Inject(AUTH_CONTEXT_CLASS)
		protected authContextClass: Type<BaseAuthContext<any>>,
	) {}

	use(req: Request, res: any, next: () => void) {
		if (!req.auth) {
			req.auth = new this.authContextClass(req);
		}

		next();
	}
}
