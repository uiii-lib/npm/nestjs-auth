import { Request } from "express";

import { BaseAuthContext } from "./base-auth.context.js";

export abstract class SessionAuthContext<T = any> extends BaseAuthContext<T> {
	constructor(req: Request) {
		super(req);

		if (!req.session) {
			throw new Error("Session is required for authentication.");
		}
	}

	async logIn(data: T) {
		await new Promise<void>((resolve, reject) =>
			this.req.session.regenerate((err) => {
				if (err) {
					reject(err);
					return;
				}

				this.req.session.auth = data;

				this.req.session.save((err) => {
					if (err) {
						reject(err);
						return;
					}

					resolve();
				});
			}),
		);
	}

	async logOut() {
		this.req.session.auth = undefined;

		await new Promise<void>((resolve, reject) => {
			this.req.session.save((err) => {
				if (err) reject(err);

				this.req.session.regenerate((err) => {
					if (err) reject(err);

					resolve();
				});
			});
		});
	}

	protected retrieveData(): T | undefined {
		return this.req.session.auth as T | undefined;
	}
}
