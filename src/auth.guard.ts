import {
	CanActivate,
	ExecutionContext,
	UnauthorizedException,
} from "@nestjs/common";

import { getRequest } from "./helpers.js";

export class AuthGuard implements CanActivate {
	canActivate(context: ExecutionContext): boolean {
		const req = getRequest(context);

		if (!req.auth.isLoggedIn()) {
			throw new UnauthorizedException();
		}

		return true;
	}
}
