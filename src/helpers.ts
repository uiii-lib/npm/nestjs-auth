import { Request } from "express";

import { ExecutionContext } from "@nestjs/common";

import { GqlContextType, GqlExecutionContext } from "@nestjs/graphql";

export function getRequest(context: ExecutionContext) {
	let req: Request;

	if (context.getType<GqlContextType>() === "graphql") {
		req = GqlExecutionContext.create(context).getContext().req;
	} else {
		req = context.switchToHttp().getRequest() as Request;
	}

	if (!req.auth) {
		throw new Error(
			`Request was not initialized for authentication. Did you forget to import SessionAuthModule?`,
		);
	}

	return req;
}
