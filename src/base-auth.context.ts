import { UnauthorizedException } from "@nestjs/common";
import { deepSelect, DeepSelector } from "@uiii-lib/utils";
import { Request } from "express";

export const AUTH_CONTEXT_CLASS = "AUTH_CONTEXT_CLASS";

export abstract class BaseAuthContext<T extends NonNullable<any>> {
	readonly publicDataSelector?: DeepSelector<T>;

	constructor(protected req: Request) {}

	get data(): T {
		const data = this.retrieveData();

		if (data === undefined) {
			throw new UnauthorizedException();
		}

		return data;
	}

	isLoggedIn(): boolean {
		return this.retrieveData() !== undefined;
	}

	abstract logIn(data: T): Promise<void>;
	abstract logOut(): Promise<void>;

	toJSON() {
		return {
			isLoggedIn: this.isLoggedIn(),
			data: deepSelect(this.retrieveData(), this.publicDataSelector),
		};
	}

	protected abstract retrieveData(): T | undefined;
}
